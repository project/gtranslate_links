
WHAT
----
This module generates a block containing images of country flags. Users 
can click these to take them to the Google Translate version of the page.
The module goes to some lengths to get the block to be shown only on
pages that are actually reachable by Google Translate. Specifically, it
does a HTTP HEAD request, as an anonymous user, on the page being visited 
to see if it can access it. If not, Google won't be able to access it 
either, and in that case it's of no use to show the block.
Also, Google Translate will not connect to HTTPS sites.

HOW
---
Configuration is done via file operations.
For each file in the 'enabledlangs' subdirectory, it will try to make a 
link, using a filename (an image of a flag, for instance) as a source of 
information, and the file itself as an image for the user to click. This
makes configuration of which languages appear and in what order trivial.

The filename maps to a language code thusly:
(freeform)_(language name)_(language code).(file extension)

where: 

freeform = Anything you want, may include underscores. You can use this
to determine the order in which the images appear (explained below).

language name = The text that pops up when you hover over a flag image.
May not include underscores.
The examples in the 'flags' directory display the popup text in locale
itself. This means you'll need unicode support to use them.

language code = The code that Google takes as the 'sl' argument on its
translation page. May not include underscores.

file extension = the file extension of your image (eg png, gif).

Ordering your links:
Links are displayed in order of alphabetical appearance of the filename.
Because of this, you can reorder the order in which the flags are displayed
by prefixing your image filenames. Consider:
01_Nederlands_nl.png
02_English_en.png
03_Svenska_sv.png

Need to put some language in between nl and en? Prefix it with 011.
For instance:
01_Nederlands_nl.png
011_Français_fr.png
02_English_en.png
03_Svenska_sv.png

Any language can be put in any position by a single rename operation.
You can devise a number of schemes making use of this alphabetical ordering.


FLAG ICONS
----------
Flag icons were extracted from KDE 3.5 (kdebase-data) so if you want to
add icons and stay consistent, that's where to look. Alternatively, you
may be able to find a set at such a place as
http://www.iconspedia.com/cat/world-flags/
but only GPL-licensed icons will be distributed with this module.
