
Requirements
------------

This module requires Drupal 5/6 and PHP5.
It also relies on proper unicode support.
It is not compatible with aggressive caching.

Upgrading
---------
Minor upgrade (eg 1.3->1.4):
Unpack new versions over your old version. This way, the contents of
your 'enabledlangs' subdirectory will be preserved and your configuration
will remain unchanged.
Major upgrade (eg 1.7->2.1) or development versions:
Check the README.TXT to see if any changes in the file naming scheme have
taken place, and act accordingly.

Installation
------------

1. Place the gtranslate_links module with all its files under
   modules/gtranslate_links.

2. Configure which languages you want to translate for by copying images
   from the 'flags' subdirectory into the 'enabledlangs' subdirectory.

3. Enable the module in admin >> site configuration >> modules 

4. Enable the 'Google Translate links' block in
   admin >> site configuration >> blocks

5. If desirable, reorder your translation links. See README.txt on how
   to do that.


Author
------

Wicher Minnaard <wicher@gavagai.eu>
